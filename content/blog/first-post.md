---
title: My first blog post
description: Welcome to my first blog post using the content module.
slug: first-post
img: blog-1.jpg
---

<style>
ul {
  display: block;
  margin-left: 30px;
}
li {
  color: #2c3e50;
  font-size: 11px;
  line-height: 30px;
  text-align: justify;
  letter-spacing: 1px;
  font-family: "Press Start 2P", cursive;
}
li:before {
  content: "\f120"; /* FontAwesome Unicode */
  font-family: FontAwesome;
  display: inline-block;
  margin-left: -1.3em; /* same as padding-left set on li */
  width: 1.3em; /* same as padding-left set on li */
}
</style>

# My first blog post

### Content

* First item
* Second item
* Third item
* Fourth item 

Welcome to my first blog post using content module
Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis, quo. Suscipit unde labore necessitatibus fugiat? Repellendus magnam reprehenderit, repudiandae impedit suscipit pariatur unde reiciendis aperiam dolore aliquam aut eum sapiente molestiae animi praesentium numquam, dolores quisquam exercitationem beatae nulla? Maxime.
Suscipit unde labore necessitatibus fugiat? Repellendus magnam reprehenderit, repudiandae impedit suscipit pariatur unde reiciendis aperiam dolore aliquam aut eum sapiente molestiae animi praesentium numquam, dolores quisquam exercitationem beatae nulla? Maxime.


```python

print('Hello World')

```
