export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: "static",
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Welcome Home Budd..",
    htmlAttrs: {
      lang: "en",
    },
    meta: [{ charset: "utf-8" }, { name: "viewport", content: "width=device-width, initial-scale=1" }, { hid: "description", name: "description", content: "" }],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/penguin.ico" },
      { rel:"stylesheet", href:"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"}
  ],
    script: [
      { src: "js/pageloader.js", type: "text/javascript", body: true, defer: true },
      { src: "//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js", type: "text/javascript", body: true, defer: true },
      { src: "js/main-sidebar.js", type: "text/javascript", body: true, defer: true }
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/scss/mystyles.scss"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: "~/plugins/vueTyper.js", ssr: false }, { src: "~/plugins/prism.js", mode: "client" }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxtjs/color-mode",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/buefy
    "@nuxt/content",
    [
      "nuxt-buefy",
      {
        css: false,
      },
    ],
  ],

  // Content for content section
  content: {
    liveEdit: false,
    markdown: {},
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          esModule: false,
        },
      })
    },
  },

  generate: {
    dir: "public",
  },
};
